/**
 * @file i2c_manager.cpp
 * Handler classes for Linux i2c adapter
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2018/08/20
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 *
 */

#include "i2c_manager.h"
#include "Chordata_error.h"
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <cstdint>

using namespace Chordata;
using namespace std;

I2C_Device::I2C_Device(const string& adapter, const bool doInit):
	i2c_adapter(adapter)
	{	
		//Perform i2c initialization only if a true value is passed
		if (doInit && init() != SUCCESS)
			throw IO_Error("The i2c adapter is not available on this system");
	};

i2c_result I2C_Device::init(){
	if ( ( i2c_file = open(i2c_adapter.c_str(), O_RDWR) ) < 0)
		return ADAPTER_NOT_AVAIBLE;

	return SUCCESS;
}

void I2C_io::I2CwriteByte(uint8_t address, uint8_t data){
	if (ioctl(get_adapter_i2c_file(), I2C_SLAVE, address) < 0){
		throw IO_Error("Slave device not found");

	} 


	if ( i2c_smbus_write_byte(get_adapter_i2c_file(), data) < 0){
		throw IO_Error("Cannot write to slave device");

	} 

}

void I2C_io::I2CwriteByte(uint8_t address, uint8_t subAddress, uint8_t data)
{	

	if (ioctl(get_adapter_i2c_file(), I2C_SLAVE, address) < 0){
		throw IO_Error("Slave device not found");

	} 


	if ( i2c_smbus_write_byte_data(get_adapter_i2c_file(), subAddress, data) < 0){
		throw IO_Error("Cannot set subAdress to slave device");
	
	} 

 }

int32_t I2C_io::I2CreadByte(uint8_t address, uint8_t subAddress)
{	

	// cout << "GET ADAPTER ON READBYTE " << I2C_io::getAdapter() << endl;
	if (ioctl(I2C_io::get_adapter_i2c_file(), I2C_SLAVE, address) < 0){
		throw IO_Error("Slave device not found");
	
	}


	int32_t data; // `data` will store the register data	 
	if ( ( data = i2c_smbus_read_byte_data(get_adapter_i2c_file(), subAddress) ) < 0 ){
		throw IO_Error("Can't read from the slave device");

	}

	return data;          // Return data read from slave register
}

void I2C_io::I2CreadBytes(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count)
{  	
	if (ioctl(get_adapter_i2c_file(), I2C_SLAVE, address) < 0){
		throw IO_Error("Slave device not found");

	}

	// i2c_smbus_read_i2c_block_data(int file, __u8 command, __u8 length, __u8 *values)
	if ( i2c_smbus_read_i2c_block_data(get_adapter_i2c_file(), subAddress, count, dest) < 0 ){
		throw IO_Error("Can't read from the slave device");

	}
}

