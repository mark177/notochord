/**
 * @file Chordata_node.cpp
 * In notochord the nodes represent hardware or virtual elements in the sensor tree. 
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2018/08/20
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 *
 */

#include <iostream>
#include <algorithm>
#include <condition_variable>
#include <mutex>


#include "Chordata_def.h"
#include "Chordata_node.h"
#include "Chordata_parser.h"
#include "MadgwickAHRS.h"


namespace Chordata{
	// typedef std::function<const Chordata::Configuration_Data&(void)> config_fn;

	// extern config_fn getConf;
	extern std::vector<std::array<int16_t, 3>> mag_lectures;
	extern std::array<int16_t, 3> acel_offsets;
	extern std::array<int16_t, 3> gyro_offsets;
	extern bool first_calib_done;
	extern std::mutex calibration_lock;
	extern std::condition_variable calibration_go;


	extern int K_Ceptor_addr;
}

using namespace Chordata;
using namespace std;


using AP = Chordata::Armature_Parser;
namespace comm = Chordata::Communicator;



Mux::Mux(	Link *_parent,
			tinyxml2::XMLElement *e,
			I2C_io *_i2c,
			mux_channel mch
		):
	Node(AP::getAddress(e), AP::getName(e)),
	MultiLink(_parent),
	state(mch)
	{};

Mux::Mux(	Link *_parent,
			uint8_t _address, std::string _label,
			I2C_io *_i2c,
			mux_channel mch
		):
	Node(_address, _label),
	MultiLink(_parent),
	state(mch)
	{};

Branch::Branch(	Mux *_mux,
		tinyxml2::XMLElement *e, 
		I2C_io *_i2c):
	I2C_Node(_i2c, _mux->getAddress(), AP::getName(e), _mux ),
	thisMux(_mux),
	channel(AP::getBranchCH(e))
	{};


Branch::Branch(	Mux *_mux,
		uint8_t _address, std::string _label, mux_channel _channel,
		I2C_io *_i2c):
	I2C_Node(_i2c, _address, _label, _mux ),
	thisMux(_mux),
	channel(_channel)
	{};

// K_Ceptor::K_Ceptor(	Link *_parent,
// 					tinyxml2::XMLElement *e, 
// 					I2C_io *_i2c):
// 	I2C_Node(_i2c, AP::getAddress(e), AP::getName(e), _parent),
// 	imu( new _CHORDATA_IMU_TYPE_(_i2c, AP::getAddress(e) )),
// 	osc_addr(fmt::format("{}/{}", base_osc_addr, AP::getName(e))),
// 	sensorFusion(),
// 	magOffset()
// 	{
// 		Chordata::K_Ceptor_addr = address;
// 	};

K_Ceptor::K_Ceptor(	Link *_parent,
					tinyxml2::XMLElement *e, 
					I2C_io *_i2c):
	K_Ceptor(_parent, AP::getAddress(e), AP::getName(e), _i2c )
	{};

K_Ceptor::K_Ceptor(	Link *_parent,
					uint8_t _address, std::string _label, 
					I2C_io *_i2c):
	I2C_Node(_i2c, _address, _label, _parent),
	imu( new _CHORDATA_IMU_TYPE_(_i2c, _address, Chordata::getConf().odr )),
	osc_addr(fmt::format("{}/{}", base_osc_addr, _label)),
	sensorFusion(Chordata::getConf().odr),
	magOffset(),
	mag_matrix_set(false), mag_offset_set(false), 
	acel_offset_set(false), gyro_offset_set(false)
	{
		//Set global address for calibration, this works when only one KC is attached!s
		Chordata::K_Ceptor_addr = address;

		//Set calib values to 0
		for (int i = 0; i < 3; ++i){
			magOffset[i] = 0;
			imu->aBiasRaw[i] = 0;
			imu->gBiasRaw[i] = 0;
		}

		magMatrix = {{{1,0,0}, {0,1,0}, {0,0,1}}};

	};

//static K_Ceptor data members:
const uint16_t K_Ceptor::who_am_i = EXPECTED_RESPONSE;
string K_Ceptor::base_osc_addr = "";

inline void printNineFloats(	const float& rx, const float& ry, const float& rz,
							const float& ax, const float& ay ,const float& az,
							const float& mx, const float& my, const float& mz ) {
	std::streamsize prec = cout.width(10);
	cout 	<< rx << " " << ry << " " << rz << " "
			<< ax << " " << ay << " " << az << " "
			<< mx << " " << my << " " << mz << " " << endl;

	cout.width(prec);
}

#define OFFSET_DEBUG_MSG(W, V, S)\
				comm::try_debug("{:>10} correction vector =[{:^8} {:^8} {:^8}] {}", W, V[0],V[1],V[2], S)

#define MATRIX_DEBUG_MSG(M, S)\
				comm::try_debug("{10:>10} Matrix = {9}\n"\
								"\t\t\t|{0:^12} {1:^12} {2:^12}|\n"\
								"\t\t\t|{3:^12} {4:^12} {5:^12}|\n"\
								"\t\t\t|{6:^12} {7:^12} {8:^12}|",\
								 M[0][0], M[0][1], M[0][2],\
								 M[1][0], M[1][1], M[1][2],\
								 M[2][0], M[2][1], M[2][2], S, "Mag")


void KC::correct_mag_matrix(){
	std::array<float,3> b = {imu->mx, imu->my, imu->mz };
	std::array<float,3> c = {0,0,0 };
	  for (int i=0;i<3;i++){
        for (int j=0;j<3;j++){
            c[i]+=( magMatrix[i][j]*b[j]);
        }
    }
    imu->mx = c[0];
    imu->my = c[1];
    imu->mz = c[2];
}

const uint32_t KC::EEPROM_CURRENT_VERSION  = _CHORDATA_EEPROM_CURRENT_VERSION;

void KC::bang(){
	
	if (!setup)	{
		uint16_t setup_result = 0;

		try{
			setup_result = imu->begin();
		} catch (const Chordata::IO_Error& e){
			comm::try_err("Can't find K_Ceptor {}", getLabel());
			return;
		}

		if ( setup_result == who_am_i){
			setup = true;
			comm::try_info("K_Ceptor {:.<12}: setup OK.", getLabel());
			if (Chordata::getConf().calib){
				using lock = std::lock_guard<std::mutex>;
				std::cout << "Collecting Acel and Gyro samples, don't move the KCEPTOR..." << std::endl;
				thread_sleep(Chordata::millis(250));
				while (1){
					double slope = imu->calibrateGyroAcel(_MAX_AG_CALIB_SLOPE);
					if ( slope == 0.0 ) break;
					comm::try_info("Too much variation (slope = {}) Is the sensor steady?\nRetrying..", slope);
				}
				std::copy(std::begin(imu->gBiasRaw), std::end(imu->gBiasRaw), std::begin(gyro_offsets));
				std::copy(std::begin(imu->aBiasRaw), std::end(imu->aBiasRaw), std::begin(acel_offsets));
				comm::try_info("Acel and Gyro calibration done");
				std::cout << "3.Press a key when you are ready to do the Mag calibration procedure\n"
				_CHORDATA_DEF_PROMPT
				<< std::endl;
				std::cin.ignore();
				lock lk(Chordata::calibration_lock);
				Chordata::first_calib_done = true;
				Chordata::calibration_go.notify_one();
				return;
			}

			imu->set_autoCalc();

			try{
				uint8_t eeprom_addr = (Chordata::getConf().kc_ver == 1)? R1_EEPROM_ADDR : R2_EEPROM_ADDR;
				eeprom_addr = this->getAddress() ^ eeprom_addr;

				auto validation = read_info(i2c, eeprom_addr, _CHORDATA_EEPROM_VAL_CHKSUM);

				if (validation != _CHORDATA_CRC32){
					comm::try_warn("K_Ceptor {}: No EEPROM calib data found.", getLabel());
					OFFSET_DEBUG_MSG("Gyro", imu->gBiasRaw, (gyro_offset_set)? "XML" : "NONE");
					OFFSET_DEBUG_MSG("Acel", imu->aBiasRaw, (acel_offset_set)? "XML" : "NONE");
					OFFSET_DEBUG_MSG("Mag", magOffset, (mag_offset_set)? "XML" : "NONE");
					MATRIX_DEBUG_MSG(magMatrix, (mag_matrix_set)? "XML" : "NONE");
					return;
				} 

				comm::try_debug("K_Ceptor {}: EEPROM calib data found.",getLabel());
				auto eeprom_info = read_info(i2c, eeprom_addr,  _CHORDATA_EEPROM_TIMESTAMP);
				auto eeprom_v = read_info(i2c, eeprom_addr,  _CHORDATA_EEPROM_VERSION);
				comm::try_debug("~~ V_int: {1} | Last calibration: {0} ",
					comm::timekeeper.time_str(eeprom_info),
					eeprom_v);

				if (eeprom_v < EEPROM_CURRENT_VERSION){
					comm::err("Calibration version outdated, please re-calibrate this K-Ceptor --> {} <---", 
						getLabel());
				}

				std::array<int16_t, 3> eeprom;
				//get gyro offsets
				if (!gyro_offset_set){
					eeprom = read_calib(i2c, eeprom_addr, _CHORDATA_EEPROM_GYRO_SPACE);
					std::copy(std::begin(eeprom), std::end(eeprom), std::begin(imu->gBiasRaw));
				}
				OFFSET_DEBUG_MSG("Gyro", imu->gBiasRaw, (gyro_offset_set)? "XML" : "EEPROM");
				gyro_offset_set = true;

				//get acel offsets
				if (!acel_offset_set){
					eeprom = read_calib(i2c, eeprom_addr, _CHORDATA_EEPROM_ACEL_SPACE);
					std::copy(std::begin(eeprom), std::end(eeprom), std::begin(imu->aBiasRaw));
				}
				OFFSET_DEBUG_MSG("Acel", imu->aBiasRaw, (acel_offset_set)? "XML" : "EEPROM");
				acel_offset_set = true;

				//Get mag offsets
				if (!mag_offset_set){
					eeprom = read_calib(i2c, eeprom_addr, _CHORDATA_EEPROM_MAG_SPACE);
					std::copy(std::begin(eeprom), std::end(eeprom), std::begin(magOffset));
				}
				OFFSET_DEBUG_MSG("Mag", magOffset, (mag_offset_set)? "XML" : "EEPROM");
				mag_offset_set = true;

				//Get mag matrix
				if (!mag_matrix_set){
					auto eeprom_m = read_matrix(i2c, eeprom_addr, _CHORDATA_EEPROM_MMAG_SPACE);
					setMagMatrix(eeprom_m, false);
				}
				MATRIX_DEBUG_MSG(magMatrix, (mag_matrix_set)? "XML" : "EEPROM");
				mag_matrix_set = true;

			} catch (const Chordata::IO_Error& e){
				comm::try_err("EEPROM not found on {}", getLabel());
				OFFSET_DEBUG_MSG("Gyro", imu->gBiasRaw, (gyro_offset_set)? "XML" : "NONE");
				OFFSET_DEBUG_MSG("Acel", imu->aBiasRaw, (acel_offset_set)? "XML" : "NONE");
				OFFSET_DEBUG_MSG("Mag", magOffset, (mag_offset_set)? "XML" : "NONE");
				MATRIX_DEBUG_MSG(magMatrix, (mag_matrix_set)? "XML" : "NONE");
			} //end try to read EEPROM
			
			return; //setup and EEPROM read done

		} else {
			comm::try_err("K_Ceptor {}: setup ERROR!.", getLabel());
			return;
			// throw IO_Error( "Error while performing the setup of the node");
		}
		
	}
	
	static bool beta_mangling = Chordata::getConf().fusion.beta_s != Chordata::getConf().fusion.beta_f;

	if (beta_mangling){
		if (!Chordata::getConf().state.beta_timer_init){
			Chordata::getConf().state.beta_timer_init = 1;
			comm::timekeeper.initBetaCounter(Chordata::getConf().fusion.time);
			comm::debug("INIT BETA MANGLING {} to {} in {} secs",
				Chordata::getConf().fusion.beta_s,
				Chordata::getConf().fusion.beta_f,
				Chordata::getConf().fusion.time / 1000.0f   );
		}
		//get
		float mix = comm::timekeeper.getBetaCounter();

		setBeta(Chordata::getConf().fusion.beta_f *  std::min(mix,1.0f)+
				Chordata::getConf().fusion.beta_s * (1-std::min(mix,1.0f)) );

		if (mix > 1){
			beta_mangling = false;
			comm::info_once(this, "END BETA MANGLING in {}", Chordata::getConf().fusion.beta_f);
		}
	}



	//TODO: atention, this may throw!! 
	//Check for errors!!
	try{
		imu->readGyro();
		imu->readAccel();
		imu->readMag();
		
		if (Chordata::getConf().calib){
			Chordata::mag_lectures.push_back({imu->mx, imu->my, imu->mz});
		}

		//TODO: put this in another function, and choose btw them on startup
		if (Chordata::getConf().calib || Chordata::getConf().raw){
			comm::transmit(fmt::format("{},{:d},{:d},{:d},{:d},{:d},{:d},{:d},{:d},{:d}",
				osc_addr, imu->gx, imu->gy, imu->gz, imu->ax, imu->ay, imu->az, imu->mx, imu->my, imu->mz));
			return;
		}

		//Correct magnetometer reads from callibration data
		imu->mx += magOffset[0];
		imu->my += magOffset[1];
		imu->mz += magOffset[2]; 

		correct_mag_matrix();
		//Converting Mixed axis convention on LSM9DS1 to Madgwick Right handed convention
		//see: https://www.lythaniel.fr/index.php/2016/08/20/lsm9ds1-madgwicks-ahr-filter-and-robot-orientation/
		//
		//TODO: Right now the LSM9DS1 adaptation matrix is implemented. Also use the LSM9DS0 when neccesary
		//
		// LSM9DS0:
		// 
		// gx	gy 	gz
		// ax 	ay	az
		// mx 	my 	-mz
		//   
		// LSM9DS1:
		// 
		// gy	gx 	gz
		// ay 	ax	az
		// my  -mx 	mz
		//   
		Quaternion q = MadgwickAHRSupdate(
				degToRads(imu->calcGyro(imu->gy)), 	degToRads(imu->calcGyro(imu->gx)), 	degToRads(imu->calcGyro(imu->gz)), 
				imu->calcAccel(imu->ay), 			imu->calcAccel(imu->ax), 			imu->calcAccel(imu->az), 
				imu->calcMag(imu->my), 				-imu->calcMag(imu->mx), 			imu->calcMag(imu->mz)
				);

		comm::trace("Transmit {}", getLabel());
		comm::transmit(this, move(q));
	} catch (const Chordata::IO_Error& e) {
		comm::err("Error reading {}", getLabel());
		setup = false;
	}
}

void Branch::bang(){

	for (int i = 0; i < 5; ++i){
		try{
			switchBranch();
			thisMux->setActiveChannel(channel);
			return;
		} catch (const Chordata::IO_Error& e){
			comm::err("error switching branch {}. Retrying.. ({})", getLabel(), i);
			thread_sleep(Chordata::millis(20));
		}
	}

	comm::err("Too many errors when switching branch {}.", getLabel());
	throw IO_Error("Can't write to mux");

}

void Branch::switchBranch(){
	i2c->I2CwriteByte(address, channel);
}


//////////////////////////////////
/// For testing individual utils 
//////////////////////////////////


#ifdef __NODE_TEST_

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
	I2C_Node baseNode(0, string("BASE"));

	Mux multiplexer(0xfe, string("MUX"), &baseNode);

	for (int i =0; i<5; i++){
		multiplexer.addChild(new Branch(&multiplexer, string("BRANCH")));
	}

	Link *abranch = (multiplexer.getChild(2));
	if (abranch == nullptr)
		cout << "NO TABA EL BRANCHO" << endl;

	abranch -> addChild( new K_Ceptor(0xc1, string("first ceptor"), abranch))
			.getChild()->addChild( new K_Ceptor(0xc2, string("second ceptor"), (*abranch).getChild()));

	return 0;
}


#endif