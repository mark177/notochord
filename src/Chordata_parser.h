/**
 * @file Chordata_parser.h
 * Classes here allow parsing of the command line and xml files 
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2018/08/20
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 *
 */

#ifndef __CHORDATA_PARSER_
#define __CHORDATA_PARSER_

#include <type_traits>
#include <string>
#include <tuple>
#include <array>

#include "tinyxml2/tinyxml2.h"
#include "args.hxx"
#include "fmt/core.h"

#include "Chordata_def.h"
#include "Chordata_error.h"
#include "Chordata_node.h"
#include "Chordata_timer.h"

template <typename IO>
class I_Imu;

namespace Chordata{	

	class Cmd_Line_Validator_OutputFlag{
	public:
		Cmd_Line_Validator_OutputFlag():
		first_time_here(true),
		parsed_keywords(false)
		{	};

	    void operator()(const std::string&, const std::string&, Chordata::Output_Redirect&);

	private:
	    // fmt::writer w;
		bool first_time_here;
		bool parsed_keywords;

	};

	Configuration_Data parse_cmd_line(int argc, const char **argv);	


	Configuration_Data createDefaultConfig();
	
	Configuration_Data createConfig_with_filenames(
				const std::string& file,
				const std::string& schema);


	class XML_Parser: public tinyxml2::XMLDocument {

		tinyxml2::XMLNode *baseNode;
		tinyxml2::XMLError lastError;
		std::string validation_msg;
		
		std::string filename;
		std::string validation_filename;
		
		bool validate_xml();

		void init_procedure();


	public:

		explicit XML_Parser(Configuration_Data &);

		tinyxml2::XMLNode *getBaseNode();
		
		std::string getVersion();

	};


	class Armature_Parser: public XML_Parser {
		static float min_float_value;

		using armature_ptr = std::unique_ptr<Armature>;
		
		// I_Imu<I2C_io> *imu;
		I2C_io *i2c;
		Node_Scheluder *scheluder;
		tinyxml2::XMLNode *armatureNode;

	public:
		typedef std::tuple<float,float,float> offset;
		typedef std::array<std::array<float, 3>, 3> matrix3x3;

		Armature_Parser(Node_Scheluder *n,
						Configuration_Data &conf);

		template <typename Node, typename Parent, typename XML>
		Node *make(Parent p, XML x){
			static_assert(std::is_base_of<Chordata::Node, Node>::value, 
				"The Armature parser can only make objects derived from Chordata::Node");
			if(!i2c)
				throw Chordata::Logic_Error("Can't create a Node from an Armature_Parser without a valid I2C_io "
					"pointer. \nTry calling Armature_Parser::set_io(...) before making nodes");

			return new Node(p,x, i2c);
		}

		inline void set_io(I2C_io *_i2c){
			i2c = _i2c;	
		}

		inline I2C_io *get_io() const {
			return i2c;
		}

		tinyxml2::XMLNode *getArmatureNode();

		armature_ptr getArmature(I2C_io *_i2c);
		armature_ptr scanArmature(I2C_io *_i2c);
		void scanKCeptors(Link* first, std::string branch = "");

		Link * parseNode(tinyxml2::XMLElement*, Link * , int depth = 0);

		enum node_type{
			Null = 0,
			Mux,
			Branch,
			K_Ceptor
		};

		static const char* getName(tinyxml2::XMLElement *e){return e->Attribute("Name");}

		static uint8_t getAddress(tinyxml2::XMLElement *);

		static offset getCalibration_offset(tinyxml2::XMLElement *, const std::string&);
		static matrix3x3 getCalibration_matrix(tinyxml2::XMLElement *, const std::string&);
		
		static node_type getNodeType(tinyxml2::XMLElement*);
		
		static mux_channel getBranchCH(tinyxml2::XMLElement*);
	};
}//namespace Chordata




#endif

