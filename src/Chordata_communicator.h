/**
 * @file Chordata_communicator.h
 * Declarations for the communicator namespace, and definitions of the communicator basic functions
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2018/08/20
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 *
 */

#ifndef _CHORDATA_COMMUNICATOR_
#define _CHORDATA_COMMUNICATOR_

#include <functional>
#include <string>
#include <cstdint>
#include <vector>
#include <mutex>

#include "spdlog/spdlog.h"
#include "spdlog/sinks/base_sink.h"
#include "osc/OscOutboundPacketStream.h"
#include "ip/UdpSocket.h"
#include "osc/OscTypes.h"
#include "ip/NetworkingUtils.h"

// #include "Notochord.h"
#include "Chordata_utils.h"
#include "Chordata_def.h"
#include "Chordata_error.h"

namespace Chordata{
	extern config_fn getConf;

	namespace Communicator{
		extern long log_attemps;

		typedef std::function<void(const std::string&, const Quaternion&)> transmit_fn_t;
		typedef std::function<void(const std::string&)> comm_fn_t;

		extern Chordata::Timekeeper timekeeper;

		struct Comm_Substrate{
			//A temporary structure that fills the vectors with spdlog::sinks,
			//which are then moved to the communication functions
			using redirects = std::vector< spdlog::sink_ptr >;
			redirects transmit;
			redirects error;
			redirects log;

			spdlog::level::level_enum log_level, err_level;

			Chordata::Configuration_Data config;

			explicit Comm_Substrate(const Chordata::Configuration_Data&);
		};


		class OscOut: public spdlog::sinks::base_sink <std::mutex>{
		    
		    public:
		        explicit OscOut(const Chordata::Configuration_Data& conf ):
		        ip( GetHostByName(conf.comm.ip.c_str()) ),
		        port( conf.comm.port ),
		        base_address(conf.osc.base_address),
		        comm_address(conf.osc.base_address + conf.osc.comm_address),
		        error_address(conf.osc.base_address + conf.osc.error_address),
		        transmitSocket(),
		        buffer(),
		        p(buffer, _CHORDATA_TRANSMIT_BUFFER_SIZE)
		        {
		        	transmitSocket.SetEnableBroadcast(true);
		        	transmitSocket.Connect(IpEndpointName( ip, port ));
		        };

		        void sink_it_(const spdlog::details::log_msg& msg) override;

		        void transmit(const std::string&, const Quaternion&);

		        void flush_() override;

		    private:
		    	unsigned long ip;
		    	uint16_t port;
		    	std::string base_address, comm_address, error_address;

		    	UdpSocket transmitSocket;
		    	char buffer[_CHORDATA_TRANSMIT_BUFFER_SIZE];
		    	osc::OutboundPacketStream p;
		};

		extern transmit_fn_t transmit_handler;
		
		extern comm_fn_t output_handler;

		extern comm_fn_t trace_handler;

		extern comm_fn_t debug_handler;

		extern comm_fn_t info_handler;

		extern comm_fn_t warn_handler;

		extern comm_fn_t err_handler;
		
		inline void debug(const std::string& s){
			debug_handler(s);
		}

		template <typename... Args>
		inline void debug(const char* fmt, const Args&... args){
			debug_handler(fmt::format(fmt, args...));
		}

		inline void trace(const std::string& s){
			trace_handler(s);
		}

		template <typename... Args>
		inline void trace(const char* fmt, const Args&... args){
			trace_handler(fmt::format(fmt, args...));
		}

		template <typename... Args>
		inline void try_debug(const char* fmt, const Args&... args){
			try{
				debug_handler(fmt::format(fmt, args...));
			 } catch (const std::bad_function_call&){
		    	Chordata::Communicator::log_attemps ++;
		    }
		}

		template <typename FROM>
		inline void info_once(FROM f, const std::string& s){
			static bool doit = true;
			if (!doit) return;
			doit = true;
			info_handler(s);
		}

		template <typename... Args, typename FROM>
		inline void info_once(FROM f, const char* fmt, const Args&... args){
			static bool doit = true;
			if (!doit) return;
			doit = true;


			info_handler(fmt::format(fmt, args...));
		}

		inline void info(const std::string& s){
			info_handler(s);
		}

		template <typename... Args>
		inline void info(const char* fmt, const Args&... args){
			info_handler(fmt::format(fmt, args...));
		}

		template <typename... Args>
		inline void try_info(const char* fmt, const Args&... args){
			try{
				info_handler(fmt::format(fmt, args...));
			 } catch (const std::bad_function_call&){
		    	Chordata::Communicator::log_attemps ++;
		    }
		}

		inline void warn(const std::string& s){
			warn_handler(s);
		}

		template <typename... Args>
		inline void warn(const char* fmt, const Args&... args){
			warn_handler(fmt::format(fmt, args...));
		}

		template <typename... Args>
		inline void try_warn(const char* fmt, const Args&... args){
			try{
				warn_handler(fmt::format(fmt, args...));
			 } catch (const std::bad_function_call&){
		    	Chordata::Communicator::log_attemps ++;
		    }
		}

		inline void err(const std::string& s){
			err_handler(s);
		}

		template <typename... Args>
		inline void err(const char* fmt, const Args&... args){
			err_handler(fmt::format(fmt, args...));
		}

		template <typename... Args>
		inline void try_err(const char* fmt, const Args&... args){
			try{
				err_handler(fmt::format(fmt, args...));
			 } catch (const std::bad_function_call&){
		    	Chordata::Communicator::log_attemps ++;
		    }
		}


#include <iostream>
		inline void check_transmit(const std::string& addr, const Quaternion& q){
			static uint32_t micros_to_wait = 1000000 / Chordata::getConf().comm.send_rate; 
			static Chordata::microDuration period(micros_to_wait);
			auto delta = timekeeper.get_transmision_delta(addr);

			if (delta > period){
				transmit_handler(addr, q);
				timekeeper.set_last_transmision(addr);
				// std::cout << addr << " SENT " <<  delta.count() <<  std::endl;
			} else {
				// std::cout << addr << " ***DROPPED*** " << delta.count() <<  std::endl;
			}
			
		}


		template <typename Node>
		inline void transmit (const Node *n, Quaternion&& q){
			check_transmit(n->osc_addr, q);
		}

		inline void transmit(const char *s, Quaternion&& q){
			check_transmit(s, q);
			// transmit_handler(s,q);
		}
	
		inline void transmit(const std::string& s, const Quaternion& q){
			check_transmit(s,q);
		}

		inline void transmit(const std::string& s){
			output_handler(s);
		}

		void init_communicator(Comm_Substrate&&);

		void init_communicator(const Chordata::Configuration_Data&);

		class initializers{
			using spdlevel = spdlog::level::level_enum;

			friend void init_communicator(Comm_Substrate&&);

			static auto transmit_fn(spdlevel, Comm_Substrate::redirects&&, spdlevel);

			static comm_fn_t log_fn(spdlevel, Comm_Substrate::redirects&&, spdlevel);
		
			static comm_fn_t error_fn(spdlevel, Comm_Substrate::redirects&&, spdlevel);

		};

	}//namespace Communicator

}//namespace Chordata


#endif
