/**
 * @file EEPROM.h
 * Declarations of functions to perform read/write operations on an EEPROM memory.
 * Definitions in Notochord.cpp 
 *
 * // This file belongs to some parts of this programs that were developed on the rush. 
 * // It should be completely refactored one day
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2018/08/20
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 *
 */

#include <limits>
#include <tuple>
#include <cstdint>
#include "i2c_manager.h"
#include "Chordata_utils.h"

#define R1_EEPROM_ADDR 0x50
#define R2_EEPROM_ADDR 0x51



void write_calib(I2C_io* io, uint16_t address, std::array<int16_t, 3> vals, const int offset = 0);

void write_calib(I2C_io* io, uint16_t address, std::array<float, 9> vals, const int offset = 0);

std::array<int16_t, 3> read_calib(I2C_io* io, uint16_t address, const int offset = 0);

std::array<float, 9> read_matrix(I2C_io* io, uint16_t address, int offset);

void write_info(I2C_io* io, uint16_t address, uint32_t val, const int offset = 0);

uint32_t read_info(I2C_io* io, uint16_t address,  const int offset = 0);



