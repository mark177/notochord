/**
 * @file Chordata_gpio.h
 * class for controlling the hardware GPIO pins of the device
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2018/08/20
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 *
 */

#include "bcm2835-1.56/src/bcm2835.h"

#define PIN RPI_V2_GPIO_P1_07

namespace comm = Chordata::Communicator;

namespace Chordata{
    class GPIO{
    public: 
        GPIO () {
            comm::try_debug( "Init GPIO ");
            if (!bcm2835_init()){
                throw  System_Error( 100, "Can't init the GPIO");
            }

            bcm2835_gpio_fsel(PIN, BCM2835_GPIO_FSEL_OUTP);
            
        }

        void reset_translators(){
            comm::try_debug( " Resetting i2c addr translators ");
            disable_translators();
            // wait a bit
            bcm2835_delay(100);
            enable_translators();
   

        }

        void enable_translators(){
            bcm2835_gpio_write(PIN, HIGH);
        }

        void disable_translators(){
            bcm2835_gpio_write(PIN, LOW);
        }
    };
}
