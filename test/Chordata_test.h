/**
 * @file Chordata_test.h
 * The main file for the testing procedure.
 * To be included in other test files
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2017/10/11
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 * -catch library: Copyright (c) 2012 Two Blue Cubes Ltd, under the Boost Software License, Version 1.0
 * -trompeloeil mocking framework: Copyright Björn Fahller 2014-2017 under the Boost Software License, Version 1.0
 *
 */

#ifndef __CHORDATA_TEST__
#define __CHORDATA_TEST__

#include "Chordata_def.h"

#include <string>
#include <functional>
#include <memory>
#include "Chordata_node.h"

class I2C_io;

namespace Chordata{
	class K_Ceptor;
	class Notochord;

	template <typename IO>
	class I_Imu;

}

namespace Chordata_test{
	struct Test_Helper{
	 static const std::string get_osc_addr(const Chordata::K_Ceptor * kc);

	 static void set_imu(Chordata::K_Ceptor* kc, Chordata::I_Imu<I2C_io>* imu);

	 static int spawn_threads(Chordata::Notochord *);

	 static int join_threads(Chordata::Notochord * );

	 static int inform_thread(Chordata::Notochord * , int n);
	};

	const Chordata::Configuration_Data& create_config_for_OSC();

	extern int pipefd[2];
}

#endif