/**
 * @file Chordata_xml_examples.cpp
 * Embeding xml documents to be used by the tests
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2017/10/11
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 * -catch library: Copyright (c) 2012 Two Blue Cubes Ltd, under the Boost Software License, Version 1.0
 * -trompeloeil mocking framework: Copyright Björn Fahller 2014-2017 under the Boost Software License, Version 1.0
 *
 */

#include <fstream>
#include "catch.hpp"

#include "Chordata_test_xml_examples.h"




void create_temp_file(const std::string filename, const std::string filecontent){
	INFO("Part of testing procedure: Creating a temporary xml file [" << filename << "]");

	const int dir_err = system("mkdir -p " TEMP_FILES_PATH);
		
	if (-1 == dir_err)
	{
	    FAIL("Error while testing: cannot create directory to store temporary xml files");
	}

	std::string path = std::string( TEMP_FILES_PATH )+ filename;

	std::ofstream outfile (path.c_str());

	outfile << filecontent << std::endl;

	outfile.close();
}


  
std::string get_file_contents(const char* filepath){
	using namespace std;
	string line, result;
	ifstream myfile;
	myfile.open(filepath); //throws std::ifstream::failure
	if (myfile.is_open()){
		while ( getline (myfile,line) )
		{
		  result += line + '\n';
		}
		myfile.close();
	} else {
		throw ifstream::failure("can't open the file");
	}
	return result;
}

tinyxml2::XMLDocument Chordata_test::xmlDoc;
using namespace tinyxml2;

tinyxml2::XMLElement *Chordata_test::basicConfigXML(){
	xmlDoc.Clear();
	static XMLNode * pRoot = xmlDoc.NewElement("Chordata");
	xmlDoc.InsertFirstChild(pRoot);

	static XMLElement * pElement = xmlDoc.NewElement("Armature");
	pElement->SetAttribute("Version","1.0");

	pRoot->InsertEndChild(pElement);
	return pElement;
}

tinyxml2::XMLElement *
	Chordata_test::appendElement(tinyxml2::XMLElement *parent, const char* type,const char* name, const char* id, const char* value){
		XMLElement * pElement = xmlDoc.NewElement(type);
		pElement->SetAttribute("Name",name);
		pElement->SetAttribute("Id",id);
		pElement->SetText(value);

		parent->InsertEndChild(pElement);
		return pElement;
};




tinyxml2::XMLError Chordata_test::saveXML(tinyxml2::XMLDocument &xmlDoc, const char* filepath){
	return xmlDoc.SaveFile(filepath);
}

#ifdef	__XML_EXAMPLES_TEST_UTILITIES__
#include <iostream>
int main(int argc, char const *argv[])
{	
	using namespace std;
	try{

		string p = get_file_contents("../Chordata.xml");
		cout << p << endl;
	} catch (const exception& e){
		cout << e.what() <<endl;
	}

}

#endif




std::string xml_correct("<!-- Chordata.xml -->\
<Chordata version=\"1.0\"> \
	<Configuration>\
		<Communication>\
			<Adapter>\
				" _XML_E_ADAPTER "\
			</Adapter>\
			<Ip>\
				" _XML_E_IP "\
			</Ip>\
			<Port>\
				" _XML_E_PORT "\
			</Port>\
			<Log>\
				" _XML_E_LOG "\
			</Log>	\
		</Communication>\
		<Osc>\
			<Base>\
				" _XML_E_BASE_ADDRESS "\
			</Base>\
			\
		</Osc>\
	</Configuration>\
	<Armature>\
		<Mux Name=\"MAIN\" id=\"0\">\
			0x72\
			<Branch Name=\"AnteBrazo\" id=\"1\">\
					CH_4\
					<K_Ceptor Name=\"AnteBrazoSensor\" id=\"2\">\
					0x6a\
					<calibration type=\"offset\">\
						  351.06 2674.02 -2772.45\
					</calibration>\
					<calibration type=\"matrix\">\
						 1.076917   0.072891  -0.013270\
						 0.072891   0.960695  -0.023474\
						-0.013270  -0.023474   0.972254\
					</calibration>\
				</K_Ceptor>\
			</Branch>\
			<Branch Name=\"Brazo\" id=\"3\">\
				CH_3\
				<K_Ceptor Name=\"BrazoSensor\" id=\"4\">\
					0x6b\
					<calibration type=\"offset\">\
						 236.99 -552.77 683.68\
					</calibration>\
					<calibration type=\"matrix\">\
						 0.978881  -0.013358  -0.021316\
						-0.013358   0.990111  -0.020486\
						-0.021316  -0.020486   1.032867\
					</calibration>\
				</K_Ceptor>\
			</Branch>\
			<Branch Name=\"Mano\" id=\"5\">\
				CH_2\
				<K_Ceptor Name=\"ManoSensor\" id=\"5\">\
					0x6c\
					<calibration type=\"offset\">\
						 -904.09 873.90 1275.87\
					</calibration>\
					<calibration type=\"matrix\">\
						 9.3938e-01  -3.6198e-02  -2.6104e-02\
						-3.6198e-02   1.0290e+00  -6.5024e-04\
						-2.6104e-02  -6.5024e-04   1.0366e+00\
					</calibration>\
				</K_Ceptor>\
			</Branch>\
		</Mux>\
	</Armature>\
</Chordata>");

std::string xml_noBaseNode("<!-- Chordata.xml -->\
<Armature>\
	<Mux Name='MAIN' id='0'>0x72<Branch Name='AnteBrazo' id='1'>CH_4<K_Ceptor Name='AnteBrazoSensor' id='2'>\
				0x00\
				<calibration type='offset'>\
					  351.06 2674.02 -2772.45\
				</calibration>\
				<calibration type='matrix'>\
					 1.076917   0.072891  -0.013270\
					 0.072891   0.960695  -0.023474\
					-0.013270  -0.023474   0.972254\
				</calibration>\
			</K_Ceptor>\
		</Branch>\
		<Branch Name='Brazo' id='3'>CH_3<K_Ceptor Name='BrazoSensor' id='4'>\
				0x00\
				<calibration type='offset'>\
					 236.99 -552.77 683.68\
				</calibration>\
				<calibration type='matrix'>\
					 0.978881  -0.013358  -0.021316\
					-0.013358   0.990111  -0.020486\
					-0.021316  -0.020486   1.032867\
				</calibration>\
			</K_Ceptor>\
		</Branch>\
		<Branch Name='Mano' id='5'>CH_2<K_Ceptor Name='ManoSensor' id='5'>\
				0x00\
				<calibration type='offset'>\
					 -904.09 873.90 1275.87\
				</calibration>\
				<calibration type='matrix'>\
					 9.3938e-01  -3.6198e-02  -2.6104e-02\
					-3.6198e-02   1.0290e+00  -6.5024e-04\
					-2.6104e-02  -6.5024e-04   1.0366e+00\
				</calibration>\
			</K_Ceptor>\
		</Branch>\
	</Mux>\
</Armature>");

std::string xml_noClosingTag("<!-- Chordata.xml -->\
<Chordata version='1.0'> \
	<Armature>\
		<Mux Name='MAIN' id='0'>0x72<Branch Name='AnteBrazo' id='1'>CH_4<K_Ceptor Name='AnteBrazoSensor' id='2'>\
					0x00\
					<calibration type='offset'>\
						  351.06 2674.02 -2772.45\
					</calibration>\
					<calibration type='matrix'>\
						 1.076917   0.072891  -0.013270\
						 0.072891   0.960695  -0.023474\
						-0.013270  -0.023474   0.972254\
					</calibration>\
				</K_Ceptor>\
			<Branch Name='Brazo' id='3'>CH_3<K_Ceptor Name='BrazoSensor' id='4'>\
					0x00\
					<calibration type='offset'>\
						 236.99 -552.77 683.68\
					</calibration>\
					<calibration type='matrix'>\
						 0.978881  -0.013358  -0.021316\
						-0.013358   0.990111  -0.020486\
						-0.021316  -0.020486   1.032867\
					</calibration>\
				</K_Ceptor>\
			</Branch>\
			<Branch Name='Mano' id='5'>CH_2<K_Ceptor Name='ManoSensor' id='5'>\
					0x00\
					<calibration type='offset'>\
						 -904.09 873.90 1275.87\
					</calibration>\
					<calibration type='matrix'>\
						 9.3938e-01  -3.6198e-02  -2.6104e-02\
						-3.6198e-02   1.0290e+00  -6.5024e-04\
						-2.6104e-02  -6.5024e-04   1.0366e+00\
					</calibration>\
				</K_Ceptor>\
			</Branch>\
		</Mux>\
	</Armature>\
</Chordata>");

std::string xml_noVersion("<!-- Chordata.xml -->\
<Chordata varsion='1.0'> \
	<Armature>\
		<Mux Name='MAIN' id='0'>0x72<Branch Name='AnteBrazo' id='1'>CH_4<K_Ceptor Name='AnteBrazoSensor' id='2'>\
					0x00\
					<calibration type='offset'>\
						  351.06 2674.02 -2772.45\
					</calibration>\
					<calibration type='matrix'>\
						 1.076917   0.072891  -0.013270\
						 0.072891   0.960695  -0.023474\
						-0.013270  -0.023474   0.972254\
					</calibration>\
				</K_Ceptor>\
			</Branch>\
		</Mux>\
	</Armature>\
</Chordata>");



std::string xsd_v1_0("<xs:schema\
	attributeFormDefault=\"unqualified\" \
	elementFormDefault=\"qualified\" \
	xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">\
  \
  <xs:element name=\"Chordata\" type=\"root_type\">\
    <xs:annotation>\
      <xs:documentation>Chordata.xml</xs:documentation>\
    </xs:annotation>\
  </xs:element>\
  \
  \
  <xs:complexType name=\"root_type\">\
    <xs:all>\
      <xs:element type=\"ConfigurationType\" name=\"Configuration\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>\
      <xs:element type=\"ArmatureType\" name=\"Armature\" \
      				minOccurs=\"1\" maxOccurs=\"1\"/>\
    </xs:all>\
    <xs:attribute type=\"xs:float\" name=\"version\"/>\
  </xs:complexType>\
\
  <!-- CONFIGURATION -->\
\
  <xs:complexType name=\"ConfigurationType\">\
    <xs:all>\
      <xs:element type=\"CommunicationType\" name=\"Communication\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>\
\
      <xs:element type=\"OscType\" name=\"Osc\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>      \
    </xs:all>\
  </xs:complexType>\
\
\
  <xs:complexType name=\"OscType\">\
    <xs:all>\
      <xs:element type=\"OscAddressType\" name=\"Base\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>   \
      <xs:element type=\"OscAddressType\" name=\"Error\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>\
      <xs:element type=\"OscAddressType\" name=\"Info\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>  \
\
    </xs:all>\
  </xs:complexType>\
\
  <xs:simpleType name=\"OscAddressType\">\
    <xs:restriction base=\"xs:normalizedString\">\
      <xs:whiteSpace value='collapse'/>\
      <xs:pattern value=\"/\\S+\" />\
    </xs:restriction>\
  </xs:simpleType>\
\
  \
  <xs:complexType name=\"CommunicationType\">\
    <xs:all>\
      <xs:element type=\"adapterType\" name=\"Adapter\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>     \
\
      <xs:element type=\"ipType\" name=\"Ip\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>  \
      \
      <xs:element type=\"portType\" name=\"Port\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>\
\
      <xs:element type=\"redirectionType\" name=\"Log\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>\
\
      <xs:element type=\"redirectionType\" name=\"Error\" \
              minOccurs=\"0\" maxOccurs=\"1\"/>\
\
      <xs:element type=\"redirectionType\" name=\"Transmit\" \
      minOccurs=\"0\" maxOccurs=\"1\"/>\
\
      <xs:element type=\"filenameType\" name=\"Filename\" \
      minOccurs=\"0\" maxOccurs=\"1\"/>\
\
      <xs:element type=\"verbosityType\" name=\"Verbosity\" \
      minOccurs=\"0\" maxOccurs=\"1\"/>\
\
    </xs:all>\
  </xs:complexType>\
\
  <xs:simpleType name=\"verbosityType\">\
    <xs:restriction base=\"xs:normalizedString\">\
      <xs:whiteSpace value='collapse'/>\
      <xs:pattern value=\"[0-3]\" />\
    </xs:restriction>\
  </xs:simpleType>\
\
  <xs:simpleType name=\"filenameType\">\
    <xs:restriction base=\"xs:normalizedString\">\
      <xs:whiteSpace value='collapse'/>\
      <xs:pattern value=\".*\\.{1,6}\" />\
    </xs:restriction>\
  </xs:simpleType>\
  \
  <xs:simpleType name=\"redirectionType\">\
    <xs:restriction base=\"xs:normalizedString\">\
      <xs:whiteSpace value='collapse'/>\
      <xs:pattern value=\"((none|stdout|stderr|file|osc)\\s*[,-|]?\\s*){1,4}\" />\
\
    </xs:restriction>\
  </xs:simpleType>\
\
  <xs:simpleType name=\"adapterType\">\
    <xs:restriction base=\"xs:normalizedString\">\
      <xs:whiteSpace value='collapse'/>\
      <xs:pattern value=\"/.*/.*\"/>\
    </xs:restriction>\
  </xs:simpleType>\
\
  <xs:simpleType name=\"ipType\">\
    <xs:restriction base=\"xs:normalizedString\">\
      <xs:whiteSpace value='collapse'/>\
      <xs:pattern value=\"[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\"/>\
    </xs:restriction>\
  </xs:simpleType>\
\
  <xs:simpleType name=\"portType\">\
    <xs:restriction base=\"xs:normalizedString\">\
      <xs:whiteSpace value='collapse'/>\
      <xs:pattern value=\"[1-4]?[0-9]{4}\"/>\
    </xs:restriction>\
  </xs:simpleType>\
\
  <!-- ARMATURE -->\
\
  <xs:complexType name=\"ArmatureType\">\
    <xs:choice>\
      <xs:element type=\"MuxType\" name=\"Mux\" \
      				minOccurs=\"0\" maxOccurs=\"1\"/>\
      \
      <xs:element type=\"K_CeptorType\" name=\"K_Ceptor\" \
      				minOccurs=\"0\" maxOccurs=\"1\"/>\
    </xs:choice>\
  </xs:complexType>\
\
  <xs:simpleType name=\"calibrationContentType\">\
    <xs:restriction base=\"xs:normalizedString\">\
       <!-- <xs:whiteSpace value='collapse'/> -->\
       <xs:pattern value=\"\\s*(-?[0-9]{1,5}\\.?[0-9]{0,9}(e[-|+]\\d{1,3})?\\s+){3,9}\\s*\"/>\
     </xs:restriction>\
  </xs:simpleType>\
\
  <xs:complexType name=\"calibrationType\">\
   <xs:simpleContent>\
     <xs:extension base=\"calibrationContentType\">\
       \
       <xs:attribute type=\"calibTypeAttr\" name=\"type\" use=\"required\"/>\
       \
     </xs:extension>\
   </xs:simpleContent>\
  </xs:complexType>\
\
  <xs:simpleType name=\"calibTypeAttr\">\
   <xs:restriction base=\"xs:string\">\
     <xs:enumeration value=\"offset\" />\
     <xs:enumeration value=\"matrix\" />\
   </xs:restriction>\
  </xs:simpleType>\
\
  <xs:complexType name=\"K_CeptorType\" mixed=\"true\">\
    <xs:sequence>\
      <xs:element type=\"calibrationType\" name=\"calibration\" \
      				maxOccurs=\"2\" minOccurs=\"0\"/>\
\
      <xs:element type=\"K_CeptorType\" name=\"K_Ceptor\" \
      				minOccurs=\"0\" maxOccurs=\"1\"/>\
\
      <xs:element type=\"MuxType\" name=\"Mux\" \
      				minOccurs=\"0\" maxOccurs=\"1\"/>\
    </xs:sequence>\
    <xs:attribute type=\"xs:string\" name=\"Name\" use=\"required\"/>\
    <xs:attribute type=\"xs:byte\" name=\"id\" use=\"required\"/>\
  </xs:complexType>\
\
  <xs:complexType name=\"BranchType\" mixed=\"true\">\
    <xs:all>\
      <xs:element type=\"K_CeptorType\" name=\"K_Ceptor\"\
      				minOccurs=\"0\" maxOccurs=\"1\"/>\
      \
      <xs:element type=\"MuxType\" name=\"Mux\" \
      				minOccurs=\"0\" maxOccurs=\"1\"/>\
    </xs:all>\
    <xs:attribute type=\"xs:string\" name=\"Name\" use=\"required\"/>\
    <xs:attribute type=\"xs:byte\" name=\"id\" use=\"required\"/>\
  </xs:complexType>\
  \
  <xs:complexType name=\"MuxType\" mixed=\"true\">\
    <xs:sequence>\
      <xs:element type=\"BranchType\" name=\"Branch\" maxOccurs=\"unbounded\" minOccurs=\"0\"/>\
    </xs:sequence>\
    <xs:attribute type=\"xs:string\" name=\"Name\" use=\"required\"/>\
    <xs:attribute type=\"xs:byte\" name=\"id\" use=\"required\"/>\
  </xs:complexType>\
  \
</xs:schema>"

); 