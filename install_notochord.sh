#!/bin/bash

#change the hostname
echo notochord | sudo tee /etc/hostname
#set the supplicant to connect to a WIFI network
echo -e "\n\
network={ \n\
\tssid=\"Chordata-net\"\n\
\tpsk=\"chordata\"\n\
}" | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
wpa_cli -i wlan0 reconfigure

#Install deps
sudo apt update
sudo apt install i2c-tools libboost-dev libi2c-dev libxml2-utils liboctave-dev scons git vim

#Clone the Notochord repo, and go to the [develop] branch (the one active during Beta-testing period)
git clone https://gitlab.com/chordata/notochord.git
cd notochord
git checkout develop

#Compile a debug build, using 3 cores from your raspberry (attemping to use 4 might result in an out-of-memory error)
scons -j3 debug=1



